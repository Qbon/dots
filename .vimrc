
" don't be compatible with VI, this needs to go first
" as it changes other settings as a side effect
set nocompatible

" relative line nums + absolute one under cursor
set number relativenumber

" pretty syntax
syntax enable

" enable build-in plugin for file browsing
filetype plugin on

runtime macros/matchit.vim

" recursive search
set path+=**

" display commands
set showcmd

" history
set history=1000

" 80 char limit marker
set colorcolumn=80
highlight ColorColumn ctermbg=lightgrey guibg=lightgrey

" auto indentation
set autoindent
filetype indent on

" set backup dir
" // is to use absolute path to the files
set directory=$HOME/.vim/backup//
set backupdir=$HOME/.vim/backup//
set undodir=$HOME/.vim/undo

" incremental search
set incsearch

" allow switching unsaved buffers
set hidden

" display status bar
set laststatus=2

" show line with cursor
set cursorline

" set color for visual selection
hi Visual cterm=none ctermbg=darkgrey ctermfg=cyan

" make dot work in visual mode
vnoremap . :normal.<CR>

" Move visual selection
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" :e %% show the path of the active buffer
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
