#!/bin/bash



#######################################
# public globals, modify as you wish
#######################################
VERBOSITY=6 #TODO: prefix DOTS_
ROOT=$HOME
REPO_NAME=.dots_repo
REMOTE="https://Qbon@bitbucket.org/Qbon/dots.git"
DOTS_TAG="##### my dots #####"
CONFIG_FILE="$ROOT/.bashrc"
DOTS_TOOLS=.dots.tools
VIM_DIR=$ROOT/.vim
DOT_FILES=("$ROOT/.vimrc" \
		"$ROOT/.tmux.conf" \
		"$ROOT/dots.sh")


#######################################
# private stuff
#######################################
dots_repo=$ROOT/$REPO_NAME
dots_tools=$ROOT/$DOTS_TOOLS
dots_alias_cmd="/usr/bin/git --git-dir=$dots_repo/ --work-tree=$ROOT"

# verbosity levels
lvl_quiet=0
lvl_fatal=1
lvl_err=2
lvl_warn=3
lvl_ntfy=4
lvl_info=5
lvl_dbg=6


#######################################
# Logging functions.
# Globals:
#	VERBOSITY level
# Arguments:
#	log message
# Outputs:
#	writes message to output
# Source:
#	https://www.ludovicocaldara.net/dba/bash-tips-4-use-logging-levels/
#######################################
function log(){ verb_lvl=$lvl_quiet elog "col_wht" "$@" ;}
function log_ok()    { verb_lvl=$lvl_ntfy elog "col_grn" "SUCCESS: $@" ;}
function log_warn()  { verb_lvl=$lvl_warn elog "col_ylw" "WARN: $@" ;}
function log_info()  { verb_lvl=$lvl_info elog "col_wht" "INFO: $@" ;}
function log_dbg() { verb_lvl=$lvl_dbg elog "col_wht" "DEBUG: $@" ;}
function log_err() { verb_lvl=$lvl_err elog "col_org" "ERROR: $@" ;}
function log_fatal()  { verb_lvl=$lvl_fatal elog "col_red" "FATAL: $@" ;}
function log_dumpvar() { for var in $@ ; do log_dbg "$var=${!var}" ; done }

function elog() {
	col_wht='\033[0;37m' # White
	col_blk='\033[0;30m' # Black - Regular
	col_org='\033[0;33m' # Orange
	col_red='\033[0;31m' # Red
	col_grn='\033[0;32m' # Green
	col_ylw='\033[0;33m' # Yellow
	col_pur='\033[0;35m' # Purple
	col_rst='\033[0m'    # Text Reset

	if [ $verb_lvl == $lvl_quiet ]; then
		echo -e ${!1}${2}${col_rst}
		return
	fi
        if [ $VERBOSITY -ge $verb_lvl ]; then
                datestring=`date +"%Y-%m-%d %H:%M:%S"`
                echo -e "$datestring - ${!1}${2}${col_rst}"
        fi
}

#######################################
# Check if config file has the dots tag.
# Globals:
#	DOTS_TAG
# Arguments:
#	config file
# Outputs:
#	true if tag found
# Source:
#######################################
function dots_tag_exist() {
	if grep -q "$DOTS_TAG" $1; then
		# return 0 == true
		return 0
	fi
	# return 1 == false
	return 1
}

#######################################
# Check if dots repo exist
# Globals:
#	-
# Arguments:
#	repo
# Outputs:
#	true if repo found
# Source:
#######################################
function dots_repo_exist() {
	if [ -d "$1" ]; then
		return 0
	fi
	return 1
}

#######################################
# Modify bashrc file
# Globals:
#	REPO_NAME
#	CONFIG_NAME
#	DOTS_TAG
#	dots_alias_cmd
# Arguments:
#	repo
# Outputs:
#	true if repo found
# Source:
#######################################
function dots_do_bashrc() {
	log_dbg ${FUNCNAME[0]}
	DOTS_ALIAS="dots"

	if [ ! -f $CONFIG_FILE ]; then
		log_fatal "$CONFIG_FILE doesn't exist"
		exit -1
	fi		

	if dots_tag_exist $CONFIG_FILE; then
  		log_warn "dots tag exist in $CONFIG_FILE"
		return	
	fi
	echo "$DOTS_TAG" >> $CONFIG_FILE
	echo "alias $DOTS_ALIAS='$dots_alias_cmd'" >> $CONFIG_FILE
	log_ok "$CONFIG_FILE modified, source it if needed"
}	
#######################################
# Init dots repo
# Globals:
#	dots_repo,
#	dots_alias_cmd
# Arguments:
#	-
# Outputs:
#	dots repo in home dir,
#	tag in config file
# Source:
#	https://www.atlassian.com/git/tutorials/dotfiles
#######################################
function dots_git_init() {
	log_dbg ${FUNCNAME[0]}

	if ! dots_repo_exist "$dots_repo" ; then
		log_info "Init git bare repo: $dots_repo"
		git init --bare $dots_repo
	fi
	pushd $dots_repo
	$dots_alias_cmd config --local status.showUntrackedFiles no
	popd
	log_ok "git setup done"
}

#######################################
# Remove dots repo
# Globals:
#	dots_repo,
# Arguments:
#	-
# Outputs:
#	removed dots repo
# Source:
#######################################
function dots_git_remove() {
	log_dbg ${FUNCNAME[0]}
	
	if dots_repo_exist "$dots_repo"; then
		log_info "removing $dots_repo"
		rm -rf "$dots_repo"
	else
		log_err "$dots_repo doesn't exist"
		return
	fi
}

#######################################
# Add remote to dots repo
# Globals:
#	REMOTE,
#	dots_alias_cmd
# Arguments:
#	-
# Outputs:
#	remote added to local repo
# Source:
#######################################
function dots_git_init_remote() {
	log_dbg ${FUNCNAME[0]}
	log_info "Adding repo to remote: $REMOTE"
	$dots_alias_cmd remote add origin $REMOTE
}

#######################################
# Print help
# Globals:
#	-
# Arguments:
#	-
# Outputs:
#	help menu
# Source:
#######################################
function usage() {
	script=$(basename $BASH_SOURCE)
	log "$script <options>"
	log
	log "Options:"
	log " -i|--init: init the local repo and remote"
	log " -is|--install-stuff: install software, create dirs"
	log " -b|--backup: backup dot files"
	log " -rm|--remove-dots: remove dots and repo"
	log " -p|--print: print settings"
	log " -?|--help: display this help"
	log
	log "This script is supposed to run on a Debian/Ubuntu machine (apt is needed)"
	log "Setting up on a new machine"
	log " sudo apt install git"
	log " mkdir \$HOME/$REPO_NAME"
	log " git clone --bare $REMOTE \$HOME/$REPO_NAME"
	log " alias dots='/usr/bin/git --git-dir=\$HOME/$REPO_NAME --work-tree=\$HOME'"
	log " dots checkout"
	log " ./dots.sh -i"
}

#######################################
# Print dots info
# Globals:
#	VERBOSITY level
#	root directory
#	dots repo directory name
#	full path to dots repo
#	git alias
#	git remote
#	tag used to mark dot data
#	config file	
#	dot files under rev control
# Arguments:
#	-
# Outputs:
#	help menu
# Source:
#######################################
function dots_print_info() {
	log_dbg ${FUNCNAME[0]}
	 	
	log "verbosity=$VERBOSITY"
	log "dots home dir: $ROOT"
	log "dots repo dir: $REPO_NAME"
	log "full path: $dots_repo"
	log "git alias: $dots_alias_cmd"
	log "remote: $REMOTE"
	log "dots tag: $DOTS_TAG"
	log "config file: $CONFIG_FILE"
	log "dot files: ${DOT_FILES[*]}"
}

#######################################
# Backup dot file
# Globals:
#	ROOT - directory
#	DOT_FILES - list of files
# Arguments:
#	-
# Outputs:
#	Backup files
# Source:
#######################################
dots_backup_files() {
	log_dbg ${FUNCNAME[0]}

	bak_dir=$ROOT/dots_bak
	if [ -d "$bak_dir" ]; then
		log_warn "Removing old backup dir"
		rm -rf $bak_dir
	fi
	mkdir $bak_dir
	for i in "${DOT_FILES[@]}"; do
		log_dbg "copying file: $i to $bak_dir"
		cp $i $bak_dir
	done
}

#######################################
# Add base16 entry to config file
# Globals:
#	CONFIG_FILE
#	dots_tools
# Arguments:
#	-
# Outputs:
#	Added base16 entry to config
# Source: https://github.com/chriskempson/base16-shell
#######################################
function dots_colors_add_to_config() {
	log_dbg ${FUNCNAME[0]}

	if [ ! -f $CONFIG_FILE ]; then
		log_fatal "$CONFIG_FILE doesn't exist"
		exit -1
	fi

	if ! dots_tag_exist $CONFIG_FILE; then
		log_warn "dots tag doesn't exist in $CONFIG_FILE"
	fi
#TODO: create separate file in dots.tools and source it in bashrc
	echo BASE16_SHELL="$dots_tools/base16-shell/" >> $CONFIG_FILE
	echo '[ -n "$PS1" ] && \' >> $CONFIG_FILE
	echo '[ -s "$BASE16_SHELL/profile_helper.sh" ] && \' >> $CONFIG_FILE
	echo 'eval "$("$BASE16_SHELL/profile_helper.sh")"' >> $CONFIG_FILE
	log "you might wanna source $CONFIG_FILE"
}

#######################################
# Install color schemes (base16)
# Globals:
#	ROOT
# Arguments:
#	-
# Outputs:
#	Checkout color repo
# Source: https://github.com/chriskempson/base16-shell.git
#######################################
function dots_colors_install() {
	log_dbg ${FUNCNAME[0]}

	color_dir_root=$dots_tools
	color_dir=base16-shell
	dots_color_dir=$color_dir_root/$color_dir
	if [ -d $dots_color_dir ]; then
		log_warn "$dots_color_dir directory exist"
		return
	fi
	color_dir_repo=https://github.com/chriskempson/base16-shell.git
	git clone $color_dir_repo $dots_color_dir
	dots_colors_add_to_config
}

#######################################
# Create specific dir inside .vim
# Globals:
#	VIM_DIR
# Arguments:
#	-
# Outputs:
#	specific .vim backup dir
#######################################
function dots_create_vim_dir() {
	log_dbg ${FUNCNAME[0]}
	vim_bak_dir=$VIM_DIR/$1

	if [ -d $vim_bak_dir ]; then
		log_warn "$vim_bak_dir exists"
		return
	fi
	pushd $VIM_DIR
	mkdir -p $1
	log_info "Created $1 dir"
	popd
}
#######################################
# Create backup dirs inside .vim
# Globals:
#	VIM_DIR
# Arguments:
#	-
# Outputs:
#	.vim dir and its subfolders
#######################################
function dots_create_vim_dirs() {
	log_dbg ${FUNCNAME[0]}

	bak_dir=backup
	swp_dir=swp
	undo_dir=undo

	if [ ! -d $VIM_DIR ]; then
		log_info "Creating $VIM_DIR"
		mkdir -p $VIM_DIR
	fi
	dots_create_vim_dir $bak_dir
	dots_create_vim_dir $swp_dir
	dots_create_vim_dir $undo_dir
}

#######################################
# Install Vim Fugitive
# Globals:
#	-
# Arguments:
#	-
# Outputs:
#	Fugitive
# Source:
#	https://github.com/tpope/vim-fugitive
#######################################
function dots_vim_install_fugitive() {
	log_dbg ${FUNCNAME[0]}

	#plugins loaded automatically
	vim_plugins_auto=pack/my_plugins/start
	#plugins not loaded automatically
	vim_plugins_opt=pack/my_plugins/opt
	#check if .vim exists
	if [ ! -d $VIM_DIR ]; then
		log_err "$VIM_DIR doesn't exist"
		exit 1
	fi
	pushd $VIM_DIR
	mkdir -p $vim_plugins_auto
	mkdir -p $vim_plugins_opt
	pushd $vim_plugins_auto
	git clone https://tpope.io/vim/fugitive.git
	popd #$vim_plugins_auto
	popd #$VIM_DIR
}

#######################################
# Install software. Note: git is needed
# to checkout this script so it needs
# to be installed before.
# Globals:
#	-
# Arguments:
#	-
# Outputs:
# Source:
#######################################
function dots_install_soft() {
	log_dbg ${FUNCNAME[0]}

	sudo apt update
	sudo apt -y install tmux vim
}
#######################################
# Install stuff
# Globals:
#	-
# Arguments:
#	-
# Outputs:
# Source:
#######################################
function dots_install_stuff() {
	log_dbg ${FUNCNAME[0]}

	dots_install_soft
	dots_colors_install
	dots_create_vim_dirs
	dots_vim_install_fugitive
}

#######################################
# Process arguments passed to the script
# Globals:
#	-
# Arguments:
#	see usage
# Outputs:
#	Action depend on argument
# Source:
#######################################
function process_args(){
	log_dbg ${FUNCNAME[0]}
	while [[ $# -gt 0 ]]
	do
	key="$1"

	case $key in
		-i|--init)
		dots_do_bashrc
		dots_git_init
		#dots_git_init_remote
		exit 0
		;;
		-is|--install-stuff)
		dots_install_stuff
		exit 0
		;;
		-b|--backup)
		dots_backup_files
		exit 0
		;;
		-rm|--remove-dots)
		dots_git_remove
		exit 0
		;;
		-p|--print)
		dots_print_info
		exit 0
		;;
		-?|--help)
		usage
		exit 0
		;;
		*)
		log_err "Unknown argument $key"
		usage
		exit -1
	esac
	done
}
#todo: remove dots from .bashrc
process_args $@
